import axios, { get } from '../config'
import { usersEndpoint } from '../endpoints'

export function getUserInfo() {
	return axios(get, usersEndpoint)
}
