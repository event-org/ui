const baseurl = process.env.PUBLIC_URL || 'http://localhost'
const port = process.env.PORT || 8080

const url = `${baseurl}:${port}`

export const usersEndpoint = `${url}/users`
