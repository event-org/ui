import axios from 'axios'

const headers = {
	'content-type': 'application/json',
};

const auth = {
	username: process.env.USERNAME,
	password: process.env.PASSWORD,
};

const config = {
	headers,
	auth
}

export const [get, post, put, del] = ['get', 'post', 'put', 'delete']
const methods = [get, post, put, del]


export default function (method = 'get', url = '/', params) {
	if (!methods.includes(method)) throw new Error('Unhandled http method')
	if ([put, post].includes(method.toLowerCase())) {
		return axios[method](url, params, config)
	}
	return axios[method](url, config)
}
