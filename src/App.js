import React from 'react'
import './App.css'
import { BrowserRouter, Route, Switch } from 'react-router-dom'
import Header from './components/Header'
import Home from './routes/home'
import Login from './routes/login'
import SignUp from './routes/signup'
import Events from './routes/event'
import Profile from './routes/profile'
import AddEvents from './routes/add-event'

const NotFound = () => <div>Not found</div>

function App() {
	return (
		<>
			<BrowserRouter>
				<Header />
				<Switch>
					<Route path="/" exact={true} component={Home} />
					<Route path="/events" exact={true} component={Events} />
					<Route path="/add-events" exact={true} component={AddEvents} />
					<Route path="/login" exact={true} component={Login} />
					<Route path="/signup" exact={true} component={SignUp} />
					<Route path="/profile" exact={true} component={Profile} />
					<Route component={NotFound} />
				</Switch>
			</BrowserRouter>
		</>

	)
}

export default App;
