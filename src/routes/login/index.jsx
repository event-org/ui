import React from 'react'
import { Input, Button, Form } from 'antd'

function Login(props) {

	function hasErrors(fieldsError) {
		return Object.keys(fieldsError).some(field => fieldsError[field]);
	}

	const { getFieldDecorator, getFieldsError, getFieldError, isFieldTouched } = props.form;
	const usernameError = isFieldTouched('username') && getFieldError('username');
	const passwordError = isFieldTouched('password') && getFieldError('password');


	const handleSubmit = e => {
		e.preventDefault();
		props.form.validateFields((err, values) => {
			if (!err) {
				console.log('Received values of form: ', values);
			}
		});
	};

	return (
		<>
			<div className="content">
				<div className="content-wrapper">
					<div className="auth-wrapper">
						<div className="auth-card">
							<p>Login</p>
							<Form onSubmit={handleSubmit}>
								<Form.Item validateStatus={usernameError ? 'error' : ''} help={usernameError || ''}>
									{getFieldDecorator('username', {
										rules: [{ required: true, message: 'Please input your username!' }],
									})(
										<Input placeholder="Username" />,
									)}
								</Form.Item>
								<Form.Item validateStatus={passwordError ? 'error' : ''} help={passwordError || ''}>
									{getFieldDecorator('password', {
										rules: [{ required: true, message: 'Please input your Password!' }],
									})(
										<Input.Password placeholder="Password" />,
									)}
								</Form.Item>
								<Form.Item>
									<Button type="primary" htmlType="submit" disabled={hasErrors(getFieldsError())}>
										Log in
          				</Button>
								</Form.Item>
							</Form>
						</div>
					</div>
				</div>
			</div>
		</>
	)
}
export default Form.create({ name: "login" })(Login)
