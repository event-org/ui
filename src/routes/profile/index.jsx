import React, { useState } from 'react'
import { Row, Col, Button, Input, Form } from 'antd'

function Profile(props) {

	const initialState = {
		inputFieldDisabled: true,
		hideElement: true,
		key: 0,
	}

	const [state, setState] = useState(initialState)

	const toggleEnableFields = (inputFieldDisabled) => {
		return () => {
			const key = inputFieldDisabled ? state.key + 1 : state.key

			setState({ inputFieldDisabled, key });

		}

	}


	const handleSubmit = e => {
		e.preventDefault();
		props.form.validateFieldsAndScroll((err, values) => {
			if (!err) {
				console.log('Received values of form: ', values);
			}
		});
	}

	const handleConfirmBlur = e => {
		const { value } = e.target;
		setState({ confirmDirty: state.confirmDirty || !!value });
	}

	const compareToFirstPassword = (rule, value, callback) => {
		const { form } = props;
		if (value && value !== form.getFieldValue('password')) {
			callback('Two passwords that you enter is inconsistent!');
		} else {
			callback();
		}
	}

	const validateToNextPassword = (rule, value, callback) => {
		const { form } = props;
		if (value && state.confirmDirty) {
			form.validateFields(['confirm'], { force: true });
		}
		callback();
	}

	const { getFieldDecorator } = props.form;

	return (
		<>
			<div className="content">
				<div className="content-wrapper">
					<div className="profile-wrapper">
						<div className="profile-card">
							<p>User profile</p>
							<Form onSubmit={handleSubmit} key={state.key}>
								<Row>
									<Col span={12} style={{ paddingLeft: "2px", paddingRight: "2px" }}>
										<Form.Item >
											{getFieldDecorator('firstname', {
												rules: [
													{
														required: true,
														message: 'Please input your fistname!',
													},
												],
											})(<Input placeholder="Firstname" disabled={state.inputFieldDisabled} />)}
										</Form.Item>
									</Col>
									<Col span={12} style={{ paddingLeft: "2px", paddingRight: "2px" }}>
										<Form.Item>
											{getFieldDecorator('lastname', {
												rules: [
													{
														required: true,
														message: 'Please input your lastname!',
													},
												],
											})(<Input placeholder="Lastname" disabled={state.inputFieldDisabled} />)}
										</Form.Item>
									</Col>
								</Row>
								<Form.Item>
									{getFieldDecorator('email', {
										rules: [
											{
												type: 'email',
												message: 'The input is not valid E-mail!',
											},
											{
												required: true,
												message: 'Please input your E-mail!',
											},
										],
									})(<Input placeholder="Email" disabled={state.inputFieldDisabled} />)}
								</Form.Item>
								<Form.Item hasFeedback>
									{getFieldDecorator('password', {
										rules: [
											{
												required: true,
												message: 'Please input your password!',
											},
											{
												validator: validateToNextPassword,
											},
										],
									})(<Input.Password placeholder="Password" disabled={state.inputFieldDisabled} />)}
								</Form.Item>
								<Form.Item hasFeedback>
									{getFieldDecorator('confirm', {
										rules: [
											{
												required: true,
												message: 'Please confirm your password!',
											},
											{
												validator: compareToFirstPassword,
											},
										],
									})(<Input.Password onBlur={handleConfirmBlur} placeholder="Confirm Password" disabled={state.inputFieldDisabled} />)}
								</Form.Item>
								<Form.Item>
									<Button type="primary" htmlType="submit" onClick={toggleEnableFields(false)}>
										{state.inputFieldDisabled ? 'Edit' : 'Save'}
									</Button>
									<Button type="default" htmlType="submit" onClick={toggleEnableFields(true)} >
										Cancel
          				</Button>
								</Form.Item>
							</Form>
						</div>
					</div>
				</div>
			</div>
		</>
	)
}
export default Form.create({ name: 'profile' })(Profile)
