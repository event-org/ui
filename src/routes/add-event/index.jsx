import React, { useState } from 'react'
import { Form, Button, Input } from 'antd'

function AddEvents(props) {

	const initialState = {
		inputFieldDisabled: true,
		hideElement: true,
		key: 0,
	}

	const [state, setState] = useState(initialState)

	const toggleEnableFields = (inputFieldDisabled) => {
		return () => {
			const key = inputFieldDisabled ? state.key + 1 : state.key
			setState({ inputFieldDisabled, key })

		}

	}


	const handleSubmit = e => {
		e.preventDefault();
		props.form.validateFieldsAndScroll((err, values) => {
			if (!err) {
				console.log('Received values of form: ', values)
			}
		})
	}

	const { getFieldDecorator } = props.form

	return (
		<>
			<div className="content">
				<div className="content-wrapper">
					<div className="add-event-wrapper">
						<div className="add-event-card">
							<p>Add an event</p>
							<Form onSubmit={handleSubmit} key={state.key}>
								<Form.Item >
									{getFieldDecorator('eventName', {
										rules: [
											{
												required: true,
												message: 'Please input event name!',
											},
										],
									})(<Input placeholder="Event name" disabled={state.inputFieldDisabled} />)}
								</Form.Item>
								<Form.Item>
									{getFieldDecorator('description', {
										rules: [
											{
												required: true,
												message: 'Please input event description!',
											},
										],
									})(<Input placeholder="Description" disabled={state.inputFieldDisabled} />)}
								</Form.Item>
								<Form.Item>
									{getFieldDecorator('Slot', {
										rules: [
											{
												required: true,
												message: 'Please input Slot!',
											},
										],
									})(<Input placeholder="Slot " disabled={state.inputFieldDisabled} />)}
								</Form.Item>
								<Form.Item>
									{getFieldDecorator('location', {
										rules: [
											{
												required: true,
												message: 'Please input event location!',
											},
										],
									})(<Input placeholder="Location " disabled={state.inputFieldDisabled} />)}
								</Form.Item>
								<Form.Item>
									{getFieldDecorator('organizers', {
										rules: [
											{
												required: true,
												message: 'Please input event organizers!',
											},
										],
									})(<Input placeholder="Organizers " disabled={state.inputFieldDisabled} />)}
								</Form.Item>
								<Form.Item>
									{getFieldDecorator('contactnumber', {
										rules: [
											{
												required: true,
												message: 'Please input contact number!',
											},
										],
									})(<Input placeholder="Contact number " disabled={state.inputFieldDisabled} />)}
								</Form.Item>
								<Form.Item>
									<Button type="primary" htmlType="submit" onClick={toggleEnableFields(false)}>
										{state.inputFieldDisabled ? 'Edit' : 'Save'}
									</Button>
									<Button type="default" htmlType="submit" onClick={toggleEnableFields(true)} >
										Cancel
          				</Button>
								</Form.Item>
							</Form>
						</div>
					</div>
				</div>
			</div>
		</>
	)
}
export default Form.create({ name: 'add-events' })(AddEvents)
