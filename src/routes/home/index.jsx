import React from 'react'
import { Card, Row, Col } from 'antd'
function Home() {
	return (
		<>
			<div className="content">
				<div className="banner">
					<p>Plan, Create and Organize your event at EventOrg</p>
				</div>
				<div className="content-wrapper">
					<div className="events">
						<div className="header">
							Active events
					</div>
						<div>
							<Row type="flex" gutter={16}>
								<Col span={6} order={4}>
									<Card title="Event title" bordered={false} extra={<a href>More</a>}>
										<p>Event Description:</p>
										<p>Maximum members/slot</p>
										<p>Event date and time</p>
										<p>Event location</p>
										<p>Event host/speakers</p>
										<p>Event organizers</p>
										<p>Event organizers contact number</p>
									</Card>
								</Col>
								<Col span={6} order={3}>
									<Card title="Event title" bordered={false} extra={<a href>More</a>}>
										<p>Event Description:</p>
										<p>Maximum members/slot</p>
										<p>Event date and time</p>
										<p>Event location</p>
										<p>Event host/speakers</p>
										<p>Event organizers</p>
										<p>Event organizers contact number</p>
									</Card>
								</Col>
								<Col span={6} order={2}>
									<Card title="Event title" bordered={false} extra={<a href>More</a>}>
										<p>Event Description:</p>
										<p>Maximum members/slot</p>
										<p>Event date and time</p>
										<p>Event location</p>
										<p>Event host/speakers</p>
										<p>Event organizers</p>
										<p>Event organizers contact number</p>
									</Card>
								</Col>
								<Col span={6} order={1}>
									<Card title="Event title" bordered={false} extra={<a href>More</a>}>
										<p>Event Description:</p>
										<p>Maximum members/slot</p>
										<p>Event date and time</p>
										<p>Event location</p>
										<p>Event host/speakers</p>
										<p>Event organizers</p>
										<p>Event organizers contact number</p>
									</Card>
								</Col>
							</Row>
						</div>
					</div>
				</div>
			</div>
		</>
	)
}

export default Home;
