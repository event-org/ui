import React, { useState } from 'react'
import { Form, Input, Button, Row, Col } from 'antd'

function Signup(props) {

	const initialState = {
		confirmDirty: false,
		autoCompleteResult: [],
	};

	const [state, setState] = useState(initialState)

	const handleSubmit = e => {
		e.preventDefault();
		props.form.validateFieldsAndScroll((err, values) => {
			if (!err) {
				console.log('Received values of form: ', values);
			}
		});
	};

	const handleConfirmBlur = e => {
		const { value } = e.target;
		setState({ confirmDirty: state.confirmDirty || !!value });
	};

	const compareToFirstPassword = (rule, value, callback) => {
		const { form } = props;
		if (value && value !== form.getFieldValue('password')) {
			callback('Two passwords that you enter is inconsistent!');
		} else {
			callback();
		}
	};

	const validateToNextPassword = (rule, value, callback) => {
		const { form } = props;
		if (value && state.confirmDirty) {
			form.validateFields(['confirm'], { force: true });
		}
		callback();
	};

	const { getFieldDecorator } = props.form;

	return (
		<>
			<div className="content">
				<div className="content-wrapper">
					<div className="auth-wrapper">
						<div className="auth-card">
							<p>Sign up</p>

							<Form onSubmit={handleSubmit}>
								<Row>
									<Col span={12} style={{ paddingLeft: "2px", paddingRight: "2px" }}>
										<Form.Item >
											{getFieldDecorator('firstname', {
												rules: [
													{
														required: true,
														message: 'Please input your fistname!',
													},
												],
											})(<Input placeholder="Firstname" />)}
										</Form.Item>
									</Col>
									<Col span={12} style={{ paddingLeft: "2px", paddingRight: "2px" }}>
										<Form.Item>
											{getFieldDecorator('lastname', {
												rules: [
													{
														required: true,
														message: 'Please input your lastname!',
													},
												],
											})(<Input placeholder="Lastname" />)}
										</Form.Item>
									</Col>
								</Row>
								<Form.Item>
									{getFieldDecorator('username', {
										rules: [
											{
												required: true,
												message: 'Please input your E-mail!',
											},
										],
									})(<Input placeholder="username" />)}
								</Form.Item>
								<Form.Item>
									{getFieldDecorator('email', {
										rules: [
											{
												type: 'email',
												message: 'The input is not valid E-mail!',
											},
											{
												required: true,
												message: 'Please input your E-mail!',
											},
										],
									})(<Input placeholder="Email" />)}
								</Form.Item>
								<Form.Item hasFeedback>
									{getFieldDecorator('password', {
										rules: [
											{
												required: true,
												message: 'Please input your password!',
											},
											{
												validator: validateToNextPassword,
											},
										],
									})(<Input.Password placeholder="Password" />)}
								</Form.Item>
								<Form.Item hasFeedback>
									{getFieldDecorator('confirm', {
										rules: [
											{
												required: true,
												message: 'Please confirm your password!',
											},
											{
												validator: compareToFirstPassword,
											},
										],
									})(<Input.Password onBlur={handleConfirmBlur} placeholder="Confirm Password" />)}
								</Form.Item>
								<Form.Item>
									<Button type="primary" htmlType="submit">
										Create account
          				</Button>
								</Form.Item>
							</Form>
						</div>
					</div>
				</div>
			</div>
		</>
	)
}
export default Form.create({ name: 'signup' })(Signup)
