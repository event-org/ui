import React from 'react'
import { Layout, Menu } from 'antd';
import { BrowserRouter, Link } from 'react-router-dom'
import { withRouter } from 'react-router'
import './assets/styles.less'

const { Header } = Layout;

function Nav(props) {

	function handleRedirect(route) {
		return function (e) {
			const path = `/${route}`
			e.preventDefault()
			props.history.push(path)
		}

	}

	return (
		<Layout>
			<BrowserRouter>
				<Header className="header">
					<Link to="/" onClick={handleRedirect('')}><div className="logo" /></Link>
					<Menu
						theme="dark"
						mode="horizontal"
						defaultSelectedKeys={['2']}
						style={{ lineHeight: '64px', float: 'right' }}
					>
						<Menu.Item key="1"><Link to="/add-events" onClick={handleRedirect('add-events')}>Create an event</Link></Menu.Item>
						<Menu.Item key="2"><Link to="/login" onClick={handleRedirect('login')}>Login</Link></Menu.Item>
						<Menu.Item key="3"><Link to="/signup" onClick={handleRedirect('signup')}>Sign up</Link></Menu.Item>
						<Menu.Item key="4"><Link to="/profile" onClick={handleRedirect('profile')}>Profile</Link></Menu.Item>
						<Menu.Item key="5"><Link to="/events" onClick={handleRedirect('events')}>Events</Link></Menu.Item>
					</Menu>
				</Header>
			</BrowserRouter>
		</Layout >
	)
}

export default withRouter(Nav);
