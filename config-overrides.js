const {
  override,
  fixBabelImports,
  addLessLoader,
  removeModuleScopePlugin,
} = require('customize-cra');

module.exports = override(
  fixBabelImports('import', {
    libraryName: 'antd',
    libraryDirectory: 'es',
    style: 'css',
  }),
  addLessLoader({
    javascriptEnabled: true,
  }),
  removeModuleScopePlugin()
);
